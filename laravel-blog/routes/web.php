<?php

use Illuminate\Support\Facades\Route;

//Link to the PostController File.
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

//Solution S01 - Activity
Route::get('/', [PostController::class, 'welcome']);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// define a route wherein a view to create a post will be returned to the user.
Route::get('/posts/create', [PostController::class, 'create']);

// define a route wherein the form data will be sent via POST method to the /posts URI endpoint.
Route::post('/posts', [PostController::class, 'store']);

// define a route that will return a view containing all posts.
// this is refactored to return only the active post from the posts table.
Route::get('/posts', [PostController::class, 'index']);

// define a route that will return a view containing only the authenticated user's posts.
Route::get('/myPosts', [PostController::class, 'myPosts']);

// define a route wherein a view showing a specific post with matching URL parameter ID will be returned to the user.
Route::get('/posts/{id}', [PostController::class, 'show']);

// define a route for viewing the edit post form - S03 Activity Solution
Route::get('/posts/{id}/edit', [PostController::class, 'edit']);

// define a route that will overwrite an existing post with matching URL parameter ID via PUT method.
Route::put('/posts/{id}', [PostController::class, 'update']);

// define a route that will delete a post of the matching URL parameter ID.
// Route::delete('/posts/{id}', [PostController::class, 'destroy']);

// S04 Activity Solution
// define a route that will soft-delete a post of the matching URL parameter ID.
Route::get('/posts/{id}/archive', [PostController::class, 'archive']);

// Stretch Goal
// define a route that will reactivate a post of the matching URL parameter ID.
Route::get('/posts/{id}/unarchive', [PostController::class, 'unarchive']);

// define a route that will allow users to like post/s
Route::put('/posts/{id}/like', [PostController::class, 'like']);

// activity 
Route::post('/posts/{id}/comment', [PostController::class, 'comment']);

Route::get('/posts/{id}/viewComment', [PostController::class, 'viewComment']);



