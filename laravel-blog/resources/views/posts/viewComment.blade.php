@extends('layouts.app');

@section('content')

	<div class="card">
		<div class="card-body">
			
			<h2 class="card-title">Comment:</h2>
			
			<p class="card-text">{{$viewComment}}</p>

		</div>
	</div>

@endsection