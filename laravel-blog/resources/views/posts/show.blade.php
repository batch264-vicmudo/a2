@extends('layouts.app')

@section('content')

	<div class="card">
		<div class="card-body">
			
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted mb-3">Created at: {{$post->created_at}}</p>
			<p class="card-text">{{$post->content}}</p>

			@if(Auth::id() != $post->user_id)
			    <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
			        @method('PUT')
			        @csrf
			        @if($post->likes->contains("user_id", Auth::id()))
			            <button type="submit" class="btn btn-danger">Unlike</button>
			        @else
			            <button type="submit" class="btn btn-success">Like</button>
			        @endif
			    </form>

			    	
			    <!-- Button trigger modal -->
			    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
			      Comment
			    </button>

			    <!-- Modal -->

			    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
			     {{--  <div class="modal-dialog">

			      	<form method="POST" action="/posts/{{$post->id}}/comment">
			      		@csrf
			        <div class="modal-content">
			          <div class="modal-header">
			            <h1 class="modal-title fs-5" id="staticBackdropLabel">Post Comment</h1>
			            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			          </div>
			          <div class="modal-body">
						<textarea class="form-control" id="content" name="content" rows="5"></textarea>
			          </div>
			          <div class="modal-footer">
			            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
			            <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Submit</button>
			          </div>
			        </div>

			    	</form>
			      </div> --}}
			      <div class="modal-dialog">
			      <div class="modal-content">
			      	<form method="POST" action="/posts/{{$post->id}}/comment">
			      		@csrf
			      		<h1>Post comment:</h1>
			      		
			      		<div class="form-group">
			      			<textarea class="form-control" id="content" name="content" rows="3"></textarea>
			      		</div>
			      		<div class="mt-2">
			      			<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
			      			<button type="submit" class="btn btn-primary" data-bs-dismiss="modal">Submit</button>
			      		</div>
			      	</form>
			      	</div>
			      </div>
			    </div>

			@endif


			<div class="mt-3">
				<a href="/myPosts" class="card-link">View all posts</a>
			</div>

			<hr/>

			<div>
				
				<a href="/posts/{{$post->id}}/viewComment" class="btn btn-danger">View Comment</a>

			</div>

		</div>
	</div>


@endsection
