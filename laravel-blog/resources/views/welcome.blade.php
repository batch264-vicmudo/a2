@extends('layouts.app');

@section('content')
    
   <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/Logo.min.svg/2560px-Logo.min.svg.png" class="img-fluid">
   <h2 class="text-center">Featured Posts:</h2>
   @if($posts!==null)
   @foreach($posts as $post)
            <div class="card text-center">
                <div class="card-body">
                    <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>

                    <p class="card-text mb-3">Author: {{$post->user->name}}</p>
                   
                    
                </div>
            </div>
    @endforeach
    @endif

@endsection